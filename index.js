import express from 'express';
import { mkdir, readdir, appendFile, readFile, stat } from 'fs/promises';
import cors from 'cors';

const app = express();
const PORT = 8080;
const FOLDER_NAME = 'files';

app.use(express.json());
app.use(cors());

app
  .route('/api/files')
  .get(async (req, res) => {
    // Get list of all uploaded files as array of filenames
    await mkdir(FOLDER_NAME, { recursive: true });
    readdir(FOLDER_NAME)
      .then((files) => res.status(200).json({ message: 'Success', files: files }))
      .catch(() => res.status(400).json({ message: 'Client error' }));
  })
  .post(async (req, res) => {
    // Create file with params
    await mkdir(FOLDER_NAME, { recursive: true });
    appendFile(`${FOLDER_NAME}/${req.body.filename}`, req.body.content)
      .then(() => res.status(200).json({ message: 'File created successfully' }))
      .catch(() => res.status(400).json({ message: 'Please specify "content" parameter' }));
  });

app.get('/api/files/:filename', async (req, res) => {
  // Get file detailed info by specified filename
  const filePath = `${FOLDER_NAME}/${req.params.filename}`;

  Promise.all([readFile(filePath, 'utf-8'), stat(filePath)])
    .then(([fileContent, fileStat]) => {
      res.status(200).json({
        message: 'Success',
        filename: req.params.filename,
        content: fileContent,
        extension: req.params.filename.split('.').pop(),
        uploadedDate: fileStat.birthtime,
      });
    })
    .catch(() =>
      res.status(400).json({ message: `No file with ${req.params.filename} filename found` })
    );
});

app.use((err, req, res, next) => {
  res.status(500).json({ message: 'Server error' });
  next();
});

app.listen(PORT, () => console.log(`Listening to port ${PORT}`));
